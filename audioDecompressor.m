function [x] = audioDecompressor(D,windowLength,samplesToKeep)
%function to decompress
%the revertion of function audioCompressor
m=1;
x=[];
load 'audio' D;
for i=1:samplesToKeep:length(D)-samplesToKeep
    %calculating idct of D
    y=idct(D(i:i+samplesToKeep-1),windowLength);
    x(m:m+length(y)-1)=y;
    m=m+length(y);
end
end

