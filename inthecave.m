function g = inthecave(s)

% Full multiplication with loop

h2 = [0.8; zeros(8000,1); 0.7; zeros(8000,1); 0.5];
h2_A = [0.9; zeros(200,1);0.4; zeros(400,1); 0.3];
hh2=filter(h2,h2_A,[1;zeros(2^15-1,1)]);

length_s = length(s);
ord = ceil(log(length_s)/log(2));
s2 = zeros(2^ord,1);
s2(1:length_s) = s;
if length(s)>length(hh2)
  g = real(ifft(fft(s2).*fft(hh2,length(s2))));
else
  g = real(ifft(fft(s2,length(hh2)).*fft(hh2)));
end
  
g=g(1:length_s);

