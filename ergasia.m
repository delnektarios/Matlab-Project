%PROJECT MATLAB 2015 NEKTARIOS DELIGIANNAKIS 1115201200030

clear;
clc;
[y,fs]=audioread('song_cave_16bit.wav','native');
%normalizing audio
y=double(y);
y1=abs(y);
y=y./max(y1);

%play audio as it curently is
obj=audioplayer(y,fs);
play(obj);
pause(obj);
resume(obj);
stop(obj);

%filter impulse
d=zeros(1,2^18)'; 
d(1)=1;
h=inthecave_mex(d);
H=fft(h,6800220);
%inverted filter
H=1./H;
%applying inverted filter to audio file
Y=fft(y);
Y=Y.*H;
y=ifft(Y);

%checking result
obj=audioplayer(y,fs);
play(obj);
pause(obj);
resume(obj);
stop(obj);

%locating bird frequencies
x=y(100000:600000); %samples around the "desired" 5 seconds
windowlength=512;
[Y,f,t,s] = spectrogram(x,windowlength,windowlength-100,[],fs,'yaxis');
threshold=0.01;
maxi=max(max(s));
mini=max(min(min(s)),maxi*threshold/100.0);
indmin=find(s<mini);
s(indmin)=mini*ones(1,length(indmin));
indmax=find(s>maxi);
s(indmax)=maxi*ones(1,length(indmax));
figure
surf(t,f,10*log(s),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time (sec)');
ylabel('Frequency (Hz)');

%filtering bird frequencies
d = fdesign.bandstop('N,F3dB1,F3dB2',20,0.35*10^4,1.5*10^4,44100);
Hd = design(d,'butter');
fvtool(Hd);
y=filter(Hd,y);
%removing frequencies that spoil the audio (they are unnescesary)
%code copied from MATLAB Labs
wc=2*10^4/(fs/2);
N=150;
hd=ideal_lp(wc,N,0);
w_tet=(boxcar(N))';
h=hd.*w_tet;
y=filter(h,1,y);

%checking result
obj=audioplayer(y,fs);
play(obj);
pause(obj);
resume(obj);
stop(obj);

clear d; clear H; clear h; clear hd;
clear hw; clear w_tet; clear Y;

%compressing method
windowLength=8192;
%percentage of kept samples (could also be a specific value)
samplesToKeep=round(50/100*windowLength);
%samplesToKeep=4000;
D=audioCompressor(y,windowLength,samplesToKeep)';
%function to decompress
x=audioDecompressor(D,windowLength,samplesToKeep)'; 
%new audio-file normalization
x1=abs(x);
x=x./max(x1);

%checking result
obj=audioplayer(x,fs);
play(obj);
pause(obj);
resume(obj);
stop(obj);




















