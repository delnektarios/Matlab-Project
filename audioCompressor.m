function [ D ] = audioCompressor(audiofile,windowLength,samplesToKeep)
%function to compress
y=audiofile; 
m=1;
D=[];
for i=1:windowLength:length(y)-windowLength
    %calculate DCT
    %split the audio in pieces with length = windowLength
    Y=dct(y(i:i+windowLength-1));
    %locating the biggest absolute samples
    Y1=abs(Y);
    Y1=sort(Y,'descend');
    %keeping the first "samplesToKeep" samples
    X=find(Y1,samplesToKeep,'first');
    X=Y(X);
    %holding these values to the vector D
    D(m:m+length(X)-1)=X;
    m=m+length(X);
end
%saving vector D to disk
save 'audio' D;
clear X;
clear Y;
clear y;
clear Y1;
end

