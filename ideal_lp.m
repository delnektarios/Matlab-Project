function [ hd,n ] = ideal_lp( wc,N,v )
    a=(N-1)/2;
    m=[0:1:(N-1)];
    n=m-a+eps-v;
    hd=sin(wc*n)./(pi*n);
end

